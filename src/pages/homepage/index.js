import React from "react";
import List from "../../components/zadanie";

const homepage = ({title}) => {
	const myList = ["test1","test2","test3","test4"];
	return (
    <ul>
    {
    	myList.map((el, index) =>{
    		return <List key={index} name={el}/>
    	})
    }
    </ul>
  );
}
export default homepage;
